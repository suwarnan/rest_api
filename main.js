var express = require('express');
var app = express();
var db = require('./db');
var api_controller = require('./api_controller');
app.use('/object', api_controller);

var port = process.env.PORT || 3000;
var server = app.listen(port, function() {
  console.log('Express server listening on port ' + port);
});