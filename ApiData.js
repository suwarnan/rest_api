var mongoose = require('mongoose');  
var keydata = new mongoose.Schema({  
  key: String,
  value: String,
  timestamp: String
});
mongoose.model('ApiData', keydata);
module.exports = mongoose.model('ApiData');