var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');
router.use(bodyParser.urlencoded({ extended: true }));
var ApiData = require('./ApiData');

router.post('/', function (req, res) {
    ApiData.update({
    		key:req.body.key},
    		{
            key : req.body.key,
            value : req.body.value,
            timestamp : new Date().valueOf()
        	}, 
        	{ upsert: true },
        function (err, ApiData) {
            if (err) return res.status(500).send("There was a problem adding the information to the database.");
            res.status(200).send(ApiData);
        });
});

router.get('/mykey/', function (req, res) {
    ApiData.find({},{}, { sort: { 'timestamp' : -1 } }, function (err, ApiData) {
        if (err) return res.status(500).send("There was a problem finding the data.");
        res.status(200).send(ApiData[0]);
    });
});

router.get('/mykey/:timestamp', function (req, res) {
    ApiData.findOne({"timestamp":req.params.timestamp}, function (err, ApiData) {
        if (err) return res.status(500).send("There was a problem finding the key.");
        if (!ApiData) return res.status(404).send("No data found.");
        res.status(200).send(ApiData);
    });
});

module.exports = router;