POST : /object
POST new data or update by key 
Eg:	{key:5,value:90}
################################

GET : /object/mykey
responce latest key value
 Eg 
 {
    "_id": "5968be202152dd28cc7311f7",
    "key": "2",
    "value": "suwarnanss",
    "timestamp": "1500043556547",
    "__v": 0
}

################################
GET : /object/mykey/timestamp_value
Here 1500043556547 is timestamp
Eg:
/object/mykey/1500043556547
{
    "_id": "5968be202152dd28cc7311f7",
    "key": "2",
    "value": "suwarnanss",
    "timestamp": "1500043556547",
    "__v": 0
}


